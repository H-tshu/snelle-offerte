$(document).ready(function(){
  var flex = $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,
    directionNav: true,
    slideshow: false,   
    customDirectionNav: $(".progress-control a"),
    start: function(slider) {
        $('.radio').click(function(event){
            event.preventDefault();
            slider.flexAnimate(slider.getTarget("next"));
        });
    }
   
  });
  
  var lastSlide = $('.slides li').index($('.slides li:last'));
    $("a.last-step").click(function () {    
        flex.flexslider('next');
        flex.flexslider(lastSlide);
    });   

  $('.form-control .next').click(function(){
      $('.overlay').css("display", "block");
  });
   $('.question-block .radio').click(function(){
      $('.overlay').css("display", "block");
  });
   $('.overlay').click(function(){
      $('.overlay').css("display", "none");
  });
  $('.ssl-keur').hover(function(){
      $('.ssl-safe').toggleClass('display');
  });
  
   $('.nav-icon').click(function(){
      $('nav.mobile').toggleClass('display');
  });
  $(document).mousemove(function(e) {

$('.popup').css('left', (window.innerWidth/2 - $('.popup').width()/2));
$('.popup').css('top', (window.innerHeight/2 - $('.popup').height()/2));

if(e.pageY <= 5)
{

        // Show the exit popup
        $('.overlay').fadeIn();
        $('.popup').fadeIn();
}

});

$('.overlay').click(function(){
$('.overlay').fadeOut();
$('.popup').slideUp();
});
});


  